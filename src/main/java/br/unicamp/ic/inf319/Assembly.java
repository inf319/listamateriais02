package br.unicamp.ic.inf319;

import java.util.LinkedList;
import java.util.List;

/**
 * <img src="./doc-files/Assembly.png" alt="Assembly">
 *
 * @author INF319
 */
public class Assembly extends Part {

	private final List<Part> parts;

	/**
	 *
	 * @param thePartNumber
	 * @param theDescription
	 */
	public Assembly(PartNumber thePartNumber, String theDescription) {
		super(thePartNumber, theDescription);
		parts = new LinkedList<>();
	}

	/**
	 *
	 * @return
	 */
	@Override
	public double cost() {
		double totalCost = 0;
		// 1 - for opção
		// for (Iterator<Part> i = parts.iterator(); i.hasNext();) {
		// Part part = (Part) i.next();
		// totalCost += part.cost();
		// }
		// 2 - for-loop
		// for (Part part : parts) {
		// totalCost += part.cost();
		// }
		// 3 - Can use functional operations
		totalCost = parts.stream().map(part -> part.cost()).reduce(totalCost,
				(accumulator, _item) -> accumulator + _item);
		return totalCost;
	}

	/**
	 *
	 * @param thePart
	 */
	public void addPart(Part thePart) {
		parts.add(thePart);
	}

	/**
	 *
	 * @return
	 */
	public List<Part> getParts() {
		return parts;
	}

	/**
	 * 
	 * @return A parte com suas subpartes
	 */
	public String list() {
		StringBuilder list = new StringBuilder(super.list());
		list.append(" ");
		parts.stream().sorted().forEach((p) -> {
			list.append(p.list().replaceAll("\n", "\n "));
		});
		list.deleteCharAt(list.length() - 1);
		return list.toString();
	}
}
