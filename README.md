# INF319 - Lista de Materiais

## Aluno: Guilherme Kayo Shida

## Comentários

### Part

- Implementado o método list() que imprime o número, a descrição e o custo;
  - cost() é um método abstrato, portanto sabemos que as classes filhas implementarão esse método.
- Esta classe passou a implementar a interface Comparable;
  - Com isto, passamos a implementar o método compareTo() que nos permite criar uma comparação entre as partes.

### Assembly

- Implementado o método list() que irá sobrescrever o método list() de Part;
  - Este método, utiliza o método sorted() para ordenar as subpartes;
  - O método sorted() chama o método compareTo() para ordenação.
- Além disso, o aluno com a ajuda da solução do professor, imprimiu da forma correta com o auxílio dos métodos 
replaceAll() e deleteChartAt().